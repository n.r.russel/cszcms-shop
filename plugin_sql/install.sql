DELETE FROM `plugin_manager` WHERE `plugin_config_filename` = 'shop';
INSERT INTO `plugin_manager` (`plugin_manager_id`, `plugin_config_filename`, `plugin_active`, `timestamp_create`, `timestamp_update`) VALUES
('', 'shop', 0, NOW(), NOW());
DELETE FROM `general_label` WHERE `name` = 'shop_new_product' OR 
`name` = 'shop_hot_product' OR
`name` = 'shop_bestseller_product' OR
`name` = 'shop_soldout_product' OR
`name` = 'shop_see_more' OR
`name` = 'shop_product_category' OR
`name` = 'shop_cart_text' OR
`name` = 'shop_notfound' OR
`name` = 'shop_view_btn' OR
`name` = 'shop_price_txt' OR
`name` = 'shop_add_to_cart_btn' OR
`name` = 'shop_option_txt' OR
`name` = 'shop_product_code_txt' OR
`name` = 'shop_home_txt' OR
`name` = 'shop_product_search_txt' OR
`name` = 'shop_qty_txt' OR
`name` = 'shop_product_name_txt' OR
`name` = 'shop_amount_txt' OR
`name` = 'shop_clear_cart_txt' OR
`name` = 'shop_place_order_txt' OR
`name` = 'shop_order_total_txt' OR
`name` = 'shop_delete_alert' OR
`name` = 'shop_payment_btn' OR
`name` = 'shop_contact_detail_txt' OR
`name` = 'shop_your_email_login' OR
`name` = 'shop_payment_methods' OR
`name` = 'shop_bank_transfer' OR
`name` = 'shop_special_price' OR
`name` = 'shop_cancel_order_txt' OR
`name` = 'shop_success_order_txt' OR
`name` = 'shop_all_product_txt' OR
`name` = 'shop_payment_disable' OR
`name` = 'shop_gst_txt' OR
`name` = 'shop_gst_show_txt' OR
`name` = 'shop_promocode_txt' OR
`name` = 'shop_promo_show_txt' OR
`name` = 'shop_promo_wrong';
INSERT INTO `general_label` (`general_label_id`, `name`, `remark`, `lang_en`, `timestamp_update`) VALUES
('', 'shop_new_product', 'For new product label', 'New Products', '2016-09-19 16:26:59'),
('', 'shop_hot_product', 'For hot product label', 'Hot Products', '2016-09-19 16:26:59'),
('', 'shop_bestseller_product', 'For beset seller label', 'Best Seller', '2016-09-19 16:26:59'),
('', 'shop_soldout_product', 'For soldout label', 'Sold Out', '2016-09-19 16:26:59'),
('', 'shop_see_more', 'For see more button', 'See More', '2016-09-19 16:28:41'),
('', 'shop_product_category', 'For product category label', 'Products Category', '2016-09-19 16:28:41'),
('', 'shop_cart_text', 'For cart text', 'Shopping Cart', '2016-09-19 16:44:00'),
('', 'shop_notfound', 'For data not found', 'Data not found!', '2016-09-19 17:31:26'),
('', 'shop_view_btn', 'For view button', 'View', '2016-09-20 17:17:59'),
('', 'shop_price_txt', 'For price text.', 'Price', '2016-09-21 17:53:25'),
('', 'shop_add_to_cart_btn', 'For Add to cart button', 'Add to Cart', '2016-09-21 17:53:25'),
('', 'shop_option_txt', 'For option text', 'Additional Option', '2016-09-22 10:51:25'),
('', 'shop_product_code_txt', 'For product code text', 'Product Code', '2016-09-22 11:00:53'),
('', 'shop_home_txt', 'For home text', 'Home', '2016-09-22 15:24:07'),
('', 'shop_product_search_txt', 'For product search text', 'Products Search', '2016-09-22 15:47:27'),
('', 'shop_qty_txt', 'For quantity text', 'Qty', '2016-09-22 16:02:59'),
('', 'shop_product_name_txt', 'For product name text', 'Product Name', '2016-09-22 17:56:16'),
('', 'shop_amount_txt', 'For amount text', 'Amount', '2016-09-22 17:56:16'),
('', 'shop_clear_cart_txt', 'For clear cart text', 'Clear Cart', '2016-09-22 17:56:16'),
('', 'shop_place_order_txt', 'For place order text', 'Place Order', '2016-09-22 17:56:16'),
('', 'shop_order_total_txt', 'For order total text', 'Total', '2016-09-22 17:56:16'),
('', 'shop_delete_alert', 'For delete alert text', 'Do you want to do this ?', '2016-09-22 18:07:39'),
('', 'shop_payment_btn', 'For payment button text', 'Payment Now', '2016-09-23 15:23:48'),
('', 'shop_contact_detail_txt', 'For contact detail text', 'Contact Detail', '2016-09-23 15:27:55'),
('', 'shop_your_email_login', 'For your email logged in as text', 'Your email logged in as', '2016-09-23 16:55:09'),
('', 'shop_payment_methods', 'For payment methods text', 'Payment Methods', '2016-09-23 17:31:37'),
('', 'shop_bank_transfer', 'For bank transfer text', 'Bank Transfer', '2016-09-23 17:31:37'),
('', 'shop_special_price', 'For special price text', 'Special Price', '2016-09-25 19:51:06'),
('', 'shop_cancel_order_txt', 'For order cancel text', 'We are sorry! Your last transaction was cancelled.', '2016-09-26 10:53:09'),
('', 'shop_success_order_txt', 'For order success text', 'Your payment was successful! Thank you for purchase.', '2016-09-26 10:53:09'),
('', 'shop_all_product_txt', 'For all product text', 'All Products', '2016-09-26 10:53:09'),
('', 'shop_payment_disable', 'For shop payment disable text', 'Payment Disabled!', '2017-01-13 10:53:09'),
('', 'shop_gst_txt', 'For shop gst text', 'GST(VAT)', '2017-01-13 10:53:09'),
('', 'shop_gst_show_txt', 'For gst show text', 'GST(VAT) %d%%', '2017-01-13 10:53:09'),
('', 'shop_promocode_txt', 'For promo code text', 'Promotional Code', '2017-01-13 10:53:09'),
('', 'shop_promo_show_txt', 'For promotional show text', 'Promotional code %d%% discount offer!', '2017-01-13 10:53:09'),
('', 'shop_promo_wrong', 'For promotional is wrong text', 'Your promotional code invalid!', '2017-01-13 10:53:09');
DELETE FROM `user_perms` WHERE `name` = 'shop';
DELETE FROM `user_perms` WHERE `name` = 'shop settings';
INSERT INTO `user_perms` (`user_perms_id`, `name`, `definition`, `permstype`) VALUES
('', 'shop', 'For shop plugin access permission on backend', 'backend'),
('', 'shop settings', 'For shop plugin settings access permission on backend', 'backend');
DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE IF NOT EXISTS `shop_category` (
  `shop_category_id` int(11) AUTO_INCREMENT,
  `shop_category_main_id` int(11),
  `name` varchar(255),
  `url_rewrite` varchar(255),
  `keyword` varchar(255),
  `short_desc` varchar(255),
  `active` int(11),
  `arrange` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `shop_config`;
CREATE TABLE IF NOT EXISTS `shop_config` (
  `shop_config_id` int(11),
  `stat_new_show` int(11),
  `stat_hot_show` int(11),
  `stat_bestseller_show` int(11),
  `stat_soldout_show` int(11),
  `paypal_active` int(11),
  `sanbox_active` int(11),
  `paypal_email` varchar(255),
  `bank_detail` text,
  `bank_disable` int(11),
  `only_member` int(11),
  `currency_code` varchar(10),
  `gst_vat` float,
  `exclude_gst_vat` int(11),
  `seller_email` varchar(255),
  `order_subject` varchar(255),
  `order_body` text,
  `payment_subject` varchar(255),
  `payment_body` text,
  `signature` text,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
INSERT IGNORE INTO `shop_config` (`shop_config_id`, `stat_new_show`, `stat_hot_show`, `stat_bestseller_show`, `stat_soldout_show`, `paypal_active`, `sanbox_active`, `paypal_email`, `bank_detail`, `bank_disable`, `only_member`, `currency_code`, `gst_vat`, `exclude_gst_vat`, `seller_email`, `order_subject`, `order_body`, `payment_subject`, `payment_body`, `signature`, `timestamp_update`) VALUES
(1, 0, 0, 0, 0, 0, 0, '', '<h2>Bank Detail:</h2>\r\n<h4><strong>Example Bank (Test Branch)</strong></h4>\r\n<p><strong>ACC ID :</strong> 857-1531-19-9<br> <strong>ACC Name :</strong> Example Tester</p>\r\n<p><em>After transfer the payment. Please send the payslip to test@example.com</em></p>', 0, 0, 'USD', 0, 0, '', 'Thank you for your order!', '<p><strong>Dear Valued Customer,</strong></p>\r\n<p><strong></strong><br></p>\r\n<p>Thank you very much for your order. After payment successed. Your order is being processed and will be shipped to you.</p>\r\n<p><strong></strong><br></p>', 'Your payment has now been confirmed!', '<p><strong>Dear Valued Customer,</strong></p>\r\n<p><strong></strong><br></p>\r\n<p>Thank you for your order. Your payment has now been confirmed. Your order is already being processed and will be shipped to you.</p>', '<p><strong>Regards,</strong></p>\r\n<p><strong>Shopping Web Team</strong><br><strong>Tel: (001) 234 567 8910</strong><br><strong>Email: test@example.com</strong></p>', '2017-05-05 16:28:15');
DROP TABLE IF EXISTS `shop_payment`;
CREATE TABLE IF NOT EXISTS `shop_payment` (
  `shop_payment_id` int(11) AUTO_INCREMENT,
  `sha1_hash` varchar(255),
  `inv_id` varchar(100),
  `email` varchar(255),
  `name` varchar(255),
  `phone` varchar(255),
  `address` text,
  `payment_methods` varchar(100),
  `price_total` double,
  `order_detail` text,
  `user_agent` varchar(255),
  `ip_address` varchar(100),
  `payment_status` varchar(100),
  `shipping` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_payment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `shop_product`;
CREATE TABLE IF NOT EXISTS `shop_product` (
  `shop_product_id` int(11) AUTO_INCREMENT,
  `product_name` varchar(255),
  `url_rewrite` varchar(255),
  `shop_category_id` int(11),
  `keyword` varchar(255),
  `short_desc` varchar(255),
  `full_desc` text,
  `price` double,
  `discount` double,
  `stock` int(11),
  `product_code` varchar(100),
  `product_status` varchar(255),
  `active` int(11),
  `fb_comment_active` int(11),
  `fb_comment_limit` int(11),
  `fb_comment_sort` varchar(20),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `shop_product_imgs`;
CREATE TABLE IF NOT EXISTS `shop_product_imgs` (
  `shop_product_imgs_id` int(11) AUTO_INCREMENT,
  `shop_product_id` int(11),
  `file_upload` varchar(255),
  `caption` varchar(255),
  `arrange` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_product_imgs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `shop_product_option`;
CREATE TABLE IF NOT EXISTS `shop_product_option` (
  `shop_product_option_id` int(11) AUTO_INCREMENT,
  `shop_product_id` int(11),
  `field_type` varchar(100),
  `field_name` varchar(255),
  `field_placeholder` varchar(255),
  `field_value` varchar(255),
  `field_label` varchar(255),
  `field_sel_value` text,
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_product_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `shop_shipping`;
CREATE TABLE IF NOT EXISTS `shop_shipping` (
  `shop_shipping_id` int(11) AUTO_INCREMENT,
  `inv_id` varchar(100),
  `shipping_name` varchar(255),
  `shipping_id` varchar(100),
  `note` text,
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_shipping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
DROP TABLE IF EXISTS `shop_promocode`;
CREATE TABLE IF NOT EXISTS `shop_promocode` (
  `shop_promocode_id` int(11),
  `promocode` varchar(255),
  `remark` varchar(255),
  `start_date` date,
  `end_date` date,
  `discount` float,
  `active` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_promocode_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;