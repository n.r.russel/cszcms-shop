<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Shopping Cart Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Shopping Cart
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/cart.html
 * @deprecated	3.0.0	This class is too specific for CI.
 */
class MY_Cart extends CI_Cart {

    var $promo_discount = 0;
    var $gst_vat = 0;
    protected $_cart_extra = array();

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->product_name_rules = '\d\D';
        $this->_cart_extra = $this->CI->session->userdata('cart_extra');
        if ($this->_cart_extra === NULL) {
            // No cart exists so we'll set some base values
            $this->_cart_extra = array('promo_discount' => 0, 'gst_vat' => 0);
        }
    }

    public function setPromoDiscount($percent) {
        if ($percent > 0) {
            //$this->promo_discount = $percent;
            $this->_cart_extra['promo_discount'] = $percent;
            $this->CI->session->set_userdata(array('cart_extra' => $this->_cart_extra));
        }
    }

    public function setGstVat($percent) {
        if ($percent > 0) {
            //$this->gst_vat = $percent;
            $this->_cart_extra['gst_vat'] = $percent;
            $this->CI->session->set_userdata(array('cart_extra' => $this->_cart_extra));
        }
    }

    /**
     * Cart Total
     *
     * @return	int
     */
    public function total() {
        $discount = 0;
        $gst_vat = 0;
        if ($this->_cart_extra['promo_discount'] > 0) {
            $discount = ($this->_cart_contents['cart_total'] * $this->_cart_extra['promo_discount']) / 100;
        }
        $total = ($this->_cart_contents['cart_total']) - ($discount);
        if ($this->_cart_extra['gst_vat'] > 0) {
            $gst_vat = ($total * $this->_cart_extra['gst_vat']) / 100;
        }
        return ($total) + ($gst_vat);
    }

    /**
     * Destroy the cart
     *
     * Empties the cart and kills the session
     *
     * @return	void
     */
    public function destroy() {
        $this->_cart_contents = array('cart_total' => 0, 'total_items' => 0);
        $this->_cart_extra = array('promo_discount' => 0, 'gst_vat' => 0);
        $this->CI->session->unset_userdata('cart_contents');
        $this->CI->session->unset_userdata('cart_extra');
    }
    
    public function clearPromoGST() {
        $this->_cart_extra = array('promo_discount' => 0, 'gst_vat' => 0);
        $this->CI->session->unset_userdata('cart_extra');
    }

}
