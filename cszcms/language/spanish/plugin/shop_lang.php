<?php
//Shop Index
$lang['shop_header']                    = "Shopping System";
$lang['shop_menu']                      = "Shopping Dashboard";
$lang['shop_config']                    = "Shopping Settings";
$lang['shop_product_category']          = "Categoria de Productos";
$lang['shop_products']                  = "Productos";
$lang['shop_order']                     = "Order Management";
$lang['shop_stat_new']                  = "New Products";
$lang['shop_stat_hot']                  = "Hot Products";
$lang['shop_stat_bestseller']           = "Best Seller";
$lang['shop_stat_soldout']              = "Sold Out";
$lang['shop_paymentrecent']             = "20 Recent Payment";
$lang['shop_dashboard_totalcomplete']   = "Total Payment Complete";
$lang['shop_dashboard_totalorder']      = "Total Orders";
$lang['shop_dashboard_totalshipping']   = "Total Shipping";
$lang['shop_dashboard_totalproduct']   = "Productos Total";
$lang['shop_allproducts']                  = "Todos los Productos";

// Shop Config
$lang['shop_config_header']             = "Shopping Settings";
$lang['shop_paypal_active']             = "Paypal Active";
$lang['shop_sanbox_active']             = "Paypal Sanbox Enable";
$lang['shop_sanbox_remark']             = "Paypal Sanbox: For payment testing system from Paypal";
$lang['shop_paypal_email']             = "Paypal Business Email";
$lang['shop_bank_detail']             = "Bank Detail";
$lang['shop_currency_code']             = "Currency Code";
$lang['shop_gst_vat']             = "GST(VAT)";
$lang['shop_seller_email']             = "Seller Email";
$lang['shop_order_subject']             = "Order Email Subject";
$lang['shop_order_body']                = "Order Email Body";
$lang['shop_payment_subject']             = "Payment Email Subject";
$lang['shop_payment_body']             = "Payment Email Body";
$lang['shop_signature']                 = "Email Signature";
$lang['shop_email_header']                 = "Config de Correo Electronico";
$lang['shop_payment_header']                 = "Config de Pagos";
$lang['shop_stat_new_show']                 = "Mostrar los nuevos productos";
$lang['shop_stat_hot_show']                 = "Show Hot Products";
$lang['shop_stat_bestseller_show']                 = "Show Best Seller Products";
$lang['shop_stat_soldout_show']                 = "Show Sold Out Products";
$lang['shop_stat_remark']                 = "For shop index and Feed only!";
$lang['shop_only_member']                 = "Payment For Member Only";
$lang['shop_bank_disable']                 = "Bank Transfer Disable";
$lang['shop_exclude_gst_vat']                 = "Exclude GST(VAT)";

// Shop Category
$lang['shop_category_header']             = "Categoria de Productos";
$lang['shop_category_name']             = "Products Category Name";
$lang['shop_cat_short_desc']                = "Descripción Corta";
$lang['shop_cat_keyword']                = "Palabras claves";
$lang['shop_active']                    = "Activo";
$lang['shop_category_addnew']             = "Agregar Categoria de Productos";
$lang['shop_category_edit']             = "Editar Categoria de Productos";
$lang['shop_main_category']             = "Categoria Principal";

// Shop Products
$lang['shop_products_header']             = "Productos";
$lang['shop_products_name']             = "Nombre del Producto";
$lang['shop_products_short_desc']                = "Descripción Corta";
$lang['shop_products_full_desc']                = "Descripción Completa";
$lang['shop_products_keyword']                = "Keyword";
$lang['shop_products_price']                = "Precio";
$lang['shop_products_discount']                = "Discount";
$lang['shop_products_fullprice']                = "Full Price";
$lang['shop_products_stock']                = "Stock";
$lang['shop_products_code']                = "Codigo del Producto";
$lang['shop_products_photo']                = "Foto del Producto";
$lang['shop_products_status']                = "Estado del Producto";
$lang['shop_products_addnew']             = "Agregar Producto";
$lang['shop_products_edit']             = "Editar Producto";
$lang['shop_products_remark']           = "Upload the picture and add product options. Please edit this product after add new product.";
$lang['shop_products_upload']                = "Subir fotos";
$lang['shop_products_fileallow']              = "Solo Archivos (jpg, jpeg, png, gif). El peso de la imagen no puede ser superior a 1900px en ancho o alto.";
$lang['shop_products_caption']                = "Caption";
$lang['shop_products_options']                = "Opciones de Productos";

// Shop Order
$lang['shop_payment_status']             = "Estado del pago";
$lang['shop_order_detail']             = "Detalle de la orden";
$lang['shop_payment_Completed']             = "Completed";
$lang['shop_payment_Pending']             = "Pendiente";
$lang['shop_payment_Refunded']             = "Refunded";
$lang['shop_payment_Canceled']             = "Cancelado";

// Shop Shipping
$lang['shop_shipping_header']             = "Shipping";
$lang['shop_shipping_create']             = "Create Shipping";
$lang['shop_shipping_edit']             = "Editar Shipping";
$lang['shop_shipping_name']             = "Shipping Name";
$lang['shop_shipping_id']             = "Shipping ID";
$lang['shop_shipping_note']             = "Nota";

// Shop promotion code
$lang['shop_promo_code']             = "Promo Code";
$lang['shop_promo_text']             = "Promo Code";
$lang['shop_promo_discount']             = "Discount";
$lang['shop_promo_expired']            = "Promo Expired";
$lang['shop_promo_addnew']             = "Add Promo Code";
$lang['shop_promo_edit']             = "Edit Promo Code";
