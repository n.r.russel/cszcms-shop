<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* For your plugin config.
 * Importnt! Please Don't change the config item index name (for systems config)
 * 
 * For load plugin config:
 * $this->Csz_model->getPluginConfig('plugin_config_filename', 'item index name');
 * Ex. $this->Csz_model->getPluginConfig('article', 'plugin_name');
 * 
 */

/* Start System Config (Important) */
/* General Config */
$plugin_config['plugin_name']  = 'Shopping';
$plugin_config['plugin_urlrewrite']  = 'shop'; /* Please don't have any blank space */
$plugin_config['plugin_author']  = 'CSKAZA'; /* For your name */
$plugin_config['plugin_version']   = '1.0.8';
$plugin_config['plugin_description']   = 'Shopping system with cart'; /* For your plugin description */

/* for menu inside member zone. If not have please blank. 
 * Example: $plugin_config['plugin_member_menu'] = 'link_name';
 * The link automatic to {base_url}/plugin/{your_plugin_urlrewrite}
 * plugin_menu_permission_name is permission name from user_perms table on DB
 */
$plugin_config['plugin_member_menu'] = 'Shopping';
$plugin_config['plugin_menu_permission_name'] = '';

/* Database Config */
$plugin_config['plugin_db_table']   = array(
    'shop_product',
    'shop_category',
    'shop_config',
    'shop_payment',
    'shop_product_imgs',
    'shop_product_option',
    'shop_shipping',
    'shop_promocode',
); /* Please input all your pludin db table name */

/* Sitemap Generater Config (for content view page only) 
 * If don't want to use sitemap for your plugin. Please blank.
 */
$plugin_config['plugin_sitemap_viewtable']   = 'shop_product';
/* for sitemap sql extra condition for this view table. If not have please blank. */
$plugin_config['plugin_sqlextra_condition']   = "active = '1' AND url_rewrite != ''";

/* Sitemap Generater Config (for content category page only) 
 * If don't want to use sitemap for your plugin. Please blank.
 */
$plugin_config['plugin_sitemap_cattable']   = 'shop_category';
/* for sitemap sql extra condition for this category table. If not have please blank. */
$plugin_config['plugin_sqlextra_catcondition']   = "active = '1' AND url_rewrite != ''";

/* All your plugin file path 
 * For directory please put / into the end of path.
 * Filename or Directory name is case sensitive.
 */
$plugin_config['plugin_file_path']   = array(
    FCPATH . '/photo/plugin/shop/',
    FCPATH . '/cszcms/config/plugin/shop.php',
    FCPATH . '/cszcms/controllers/admin/plugin/Shop.php',
    FCPATH . '/cszcms/models/plugin/Shop_model.php',
    FCPATH . '/cszcms/modules/plugin/controllers/Shop.php',
    FCPATH . '/cszcms/modules/plugin/views/templates/cszdefault/shop/',
    FCPATH . '/cszcms/views/admin/plugin/shop/',
);
/* End System Config (Important) */

/* Custom config (For your plugin config)
 * Please add your config after this section
 */
